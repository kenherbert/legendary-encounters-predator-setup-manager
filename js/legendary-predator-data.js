const SCENARIO = {
    NONE: -1,
    RANDOM: 0,
    PREDATOR: 1,
    PREDATOR2: 2
};

const RACE = {
    0: 'random',
    1: 'human',
    2: 'predator',
    RANDOM: 0,
    HUMAN: 1,
    PREDATOR: 2
};


const data = {
    scenarios: [
        {
            name: 'None',
            id: SCENARIO.NONE,
            canPick: false
        },
        {
            name: 'Predator',
            id: SCENARIO.PREDATOR,
            canPick: true
        },
        {
            name: 'Predator 2',
            id: SCENARIO.PREDATOR2,
            canPick: true
        }
    ],
    races: [
        {
            name: 'Human',
            id: RACE.HUMAN
        },
        {
            name: 'Predator',
            id: RACE.PREDATOR
        }
    ],
    locations: [
        {
            name: 'The Val Verdean Jungle',
            scenario: SCENARIO.PREDATOR
        },
        {
            name: 'The Streets of Los Angeles',
            scenario: SCENARIO.PREDATOR2
        }
    ],
    objectives: [
        {
            name: 'Expendable Assets',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            stage: 1
        },
        {
            name: 'Flares, Frags, and Claymores',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            stage: 2
        },
        {
            name: 'Get To The Choppa!',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            stage: 3
        },
        {
            name: 'Warzone',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            stage: 1
        },
        {
            name: 'Personal Little War',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            stage: 2
        },
        {
            name: 'Other-World Life-Form',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            stage: 3
        },
        {
            name: 'No Sport',
            scenario: SCENARIO.PREDATOR,
            race: RACE.PREDATOR,
            stage: 1
        },
        {
            name: 'Payback Time',
            scenario: SCENARIO.PREDATOR,
            race: RACE.PREDATOR,
            stage: 2
        },
        {
            name: 'What the Hell Are You?',
            scenario: SCENARIO.PREDATOR,
            race: RACE.PREDATOR,
            stage: 3
        },
        {
            name: 'Drawn by Heat and Conflict',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            stage: 1
        },
        {
            name: 'This is History',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            stage: 2
        },
        {
            name: 'A Taste for Beef',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            stage: 3
        }
    ],
    characters: [
        {
            name: 'Blain',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            classes: {
                intel: 0,
                leadership: 5,
                strength: 1,
                survival: 5,
                tech: 3
            }
        },
        {
            name: 'Dillon',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            classes: {
                intel: 0,
                leadership: 1,
                strength: 3,
                survival: 5,
                tech: 5
            }
        },
        {
            name: 'Dutch',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            classes: {
                intel: 0,
                leadership: 3,
                strength: 5,
                survival: 1,
                tech: 5
            }
        },
        {
            name: 'Mac',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            classes: {
                intel: 0,
                leadership: 5,
                strength: 5,
                survival: 3,
                tech: 1
            }
        },
        {
            name: 'Danny',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            classes: {
                intel: 5,
                leadership: 5,
                strength: 0,
                survival: 3,
                tech: 1
            }
        },
        {
            name: 'Harrigan',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            classes: {
                intel: 3,
                leadership: 5,
                strength: 0,
                survival: 1,
                tech: 5
            }
        },
        {
            name: 'Keyes',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            classes: {
                intel: 5,
                leadership: 1,
                strength: 0,
                survival: 5,
                tech: 3
            }
        },
        {
            name: 'Lambert',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            classes: {
                intel: 1,
                leadership: 3,
                strength: 0,
                survival: 5,
                tech: 5
            }
        },
        {
            name: 'Woodland - Intel',
            scenario: SCENARIO.PREDATOR,
            race: RACE.PREDATOR,
            classes: {
                intel: 14,
                leadership: 0,
                strength: 0,
                survival: 0,
                tech: 0
            }
        },
        {
            name: 'Woodland - Strength',
            scenario: SCENARIO.PREDATOR,
            race: RACE.PREDATOR,
            classes: {
                intel: 0,
                leadership: 0,
                strength: 14,
                survival: 0,
                tech: 0
            }
        },
        {
            name: 'Woodland - Survival',
            scenario: SCENARIO.PREDATOR,
            race: RACE.PREDATOR,
            classes: {
                intel: 0,
                leadership: 0,
                strength: 0,
                survival: 14,
                tech: 0
            }
        },
        {
            name: 'Woodland - Tech',
            scenario: SCENARIO.PREDATOR,
            race: RACE.PREDATOR,
            classes: {
                intel: 0,
                leadership: 0,
                strength: 0,
                survival: 0,
                tech: 14
            }
        },
        {
            name: 'Urban - Intel',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            classes: {
                intel: 14,
                leadership: 0,
                strength: 0,
                survival: 0,
                tech: 0
            }
        },
        {
            name: 'Urban - Strength',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            classes: {
                intel: 0,
                leadership: 0,
                strength: 14,
                survival: 0,
                tech: 0
            }
        },
        {
            name: 'Urban - Survival',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            classes: {
                intel: 0,
                leadership: 0,
                strength: 0,
                survival: 14,
                tech: 0
            }
        },
        {
            name: 'Urban - Tech',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            classes: {
                intel: 0,
                leadership: 0,
                strength: 0,
                survival: 0,
                tech: 14
            }
        }
    ],
    avatars: [
        {
            name: 'CIA Agent',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            roleCards: ['In The Shadows']
        },
        {
            name: 'Guerilla',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            roleCards: ['Fighting the Good Fight']
        },
        {
            name: 'Lieutenant',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            roleCards: ['Command Decisions']
        },
        {
            name: 'Radioman',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            roleCards: ['Position and Situation']
        },
        {
            name: 'Tracker',
            scenario: SCENARIO.PREDATOR,
            race: RACE.HUMAN,
            roleCards: ['It Ain\'t No Man']
        },
        {
            name: 'Detective',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            roleCards: ['Knocking on Doors']
        },
        {
            name: 'Gangster',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            roleCards: ['Running the Streets']
        },
        {
            name: 'O.W.L.F Agent',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            roleCards: ['The Highest Tech']
        },
        {
            name: 'Reporter',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            roleCards: ['Sources and Contacts']
        },
        {
            name: 'SWAT Officer',
            scenario: SCENARIO.PREDATOR2,
            race: RACE.HUMAN,
            roleCards: ['Partners in Crime']
        },
        {
            name: 'City Hunter',
            scenario: SCENARIO.PREDATOR | SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            roleCards: ['From the Other Side']
        },
        {
            name: 'Elder Hunter',
            scenario: SCENARIO.PREDATOR | SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            roleCards: ['Crafty Veteran']
        },
        {
            name: 'Gunslinger Hunter',
            scenario: SCENARIO.PREDATOR | SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            roleCards: ['Weapon Master']
        },
        {
            name: 'Jungle Hunter',
            scenario: SCENARIO.PREDATOR | SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            roleCards: ['Like a Chameleon']
        },
        {
            name: 'Primeval Hunter',
            scenario: SCENARIO.PREDATOR | SCENARIO.PREDATOR2,
            race: RACE.PREDATOR,
            roleCards: ['Honorable Warrior']
        }
    ],
    objectiveSetup: {
        1: {
            objective1Additions: 0,
            objective2Additions: 0,
            objective3Additions: 0,
            setupTurns: 0
        },
        2: {
            objective1Additions: 0,
            objective2Additions: 1,
            objective3Additions: 2,
            setupTurns: 0
        },
        3: {
            objective1Additions: 2,
            objective2Additions: 3,
            objective3Additions: 4,
            setupTurns: 0
        },
        4: {
            objective1Additions: 4,
            objective2Additions: 5,
            objective3Additions: 6,
            setupTurns: 0
        },
        5: {
            objective1Additions: 4,
            objective2Additions: 5,
            objective3Additions: 6,
            setupTurns: 1
        }
    }
};


const changelog = [
    {
        date: '2023-06-14',
        items : [
            'Tooltip triggers now accept tab focus',
            'Modified changelog to be easier to maintain',
            'Added last updated date to page'
        ]
    },
    {
        date: '2023-05-07',
        items : [
            'Initial release'
        ]
    }
];