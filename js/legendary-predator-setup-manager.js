/* global $ */


let characters = [];
let chart = null;


/**
 * Check if elements need dark mode styling
 */
function respectDarkMode() {
    if($('body').hasClass('dark-mode')) {
    } else {
    }
}


function getRandomNumber(max) {
    return Math.floor(Math.random() * max);
}


function getRandomItem(array) {
    let random = getRandomNumber(array.length);

    return array[random];
}


function saveState() {

    // Not grabbed from getOptions() so we have the raw values without effect of other selections
    const playerCount = $('input[name="player-count"]:checked').val();
    const playAs = $('input[name="play-as"]:checked').val();
    const matchScenario = $('#match-scenario').val();
    const limitLocation = $('#limit-location-yes').is(':checked') ? "yes" : "no";
    const limitObjectives = $('#limit-objectives-yes').is(':checked') ? "yes" : "no";
    const limitAvatars = $('#limit-avatars-yes').is(':checked') ? "yes" : "no";
    const limitCharacters = $('#limit-characters-yes').is(':checked') ? "yes" : "no";

    localStorage.setItem('config', JSON.stringify({
        playerCount: playerCount,
        playAs: playAs,
        matchScenario: matchScenario,
        limitLocation: limitLocation,
        limitObjectives: limitObjectives,
        limitAvatars: limitAvatars,
        limitCharacters: limitCharacters
    }));

    $('#save').removeClass('btn-dark').addClass('btn-success');

    setTimeout(() => {
        $('#save').removeClass('btn-success').addClass('btn-dark');
        $('#load').removeAttr('disabled');
    }, 600);
}


function loadState() {
    const options = JSON.parse(localStorage.getItem('config'));

    if(options !== null) {
        $('#load').removeAttr('disabled');

        if(options.playerCount) {
            $('input[name="player-count"]').removeAttr('checked').parent().removeClass('active');
            $(`input[name="player-count"][value="${options.playerCount}"]`).prop('checked', true).parent().addClass('active');
        }

        if(options.playAs) {
            $('input[name="play-as"]').removeAttr('checked').parent().removeClass('active');
            $(`#play-as-${RACE[options.playAs]}`).prop('checked', true).parent().addClass('active');
        }

        if(options.matchScenario) {
            $("#match-scenario").val(options.matchScenario);
        }

        if(options.limitLocation) {
            $('input[name="limit-location"]').removeAttr('checked').parent().removeClass('active');
            $(`#limit-location-${options.limitLocation}`).prop('checked', true).parent().addClass('active');
        }

        if(options.limitObjectives) {
            $('input[name="limit-objectives"]').removeAttr('checked').parent().removeClass('active');
            $(`#limit-objectives-${options.limitObjectives}`).prop('checked', true).parent().addClass('active');
        }

        if(options.limitAvatars) {
            $('input[name="limit-avatars"]').removeAttr('checked').parent().removeClass('active');
            $(`#limit-avatars-${options.limitAvatars}`).prop('checked', true).parent().addClass('active');
        }

        if(options.limitCharacters) {
            $('input[name="limit-characters"]').removeAttr('checked').parent().removeClass('active');
            $(`#limit-characters-${options.limitCharacters}`).prop('checked', true).parent().addClass('active');
        }

        $('#match-scenario').trigger('change');
    }
}

function getOptions() {
    const playerCount = parseInt($('input[name="player-count"]:checked').val(), 10);
    const playAs = parseInt($('input[name="play-as"]:checked').val(), 10);
    const matchScenario = parseInt($('#match-scenario').val(), 10);
    const limitLocation = (matchScenario !== SCENARIO.NONE) && $('#limit-location-yes').is(':checked');
    const limitObjectives = (matchScenario !== SCENARIO.NONE) && $('#limit-objectives-yes').is(':checked');
    const limitAvatars = (matchScenario !== SCENARIO.NONE) && $('#limit-avatars-yes').is(':checked');
    const limitCharacters = (matchScenario !== SCENARIO.NONE) && $('#limit-characters-yes').is(':checked');

    return {
        playerCount: playerCount,
        playAs: playAs,
        matchScenario: matchScenario,
        limitLocation: limitLocation,
        limitObjectives: limitObjectives,
        limitAvatars: limitAvatars,
        limitCharacters: limitCharacters
    };
}


function generateGame() {
    const options = getOptions();
    let race = null;

    if(options.playAs === RACE.RANDOM) {
        race = getRandomItem(data.races);
    } else {
        race = data.races.filter(race => race.id === options.playAs)[0];
    }

    let location = null;
    let scenario = null;
    
    if(options.matchScenario === SCENARIO.RANDOM) {
        scenario = getRandomItem(data.scenarios.filter(scenario => scenario.canPick === true));
    } else {
        scenario = data.scenarios.filter(scenario => scenario.id === options.matchScenario)[0];
    }

    if(options.limitLocation) {
        location = data.locations.filter(loc => loc.scenario === scenario.id)[0];
    } else {
        location = getRandomItem(data.locations);
    }

    $('#selected-theme').text(scenario.name);
    $('#selected-location').text(location.name);


    let objective1 = null
    let objective2 = null;
    let objective3 = null;

    if(options.limitObjectives) {
        objective1 = data.objectives.filter(obj => obj.stage === 1 && obj.race === race.id && obj.scenario === scenario.id)[0];
        objective2 = data.objectives.filter(obj => obj.stage === 2 && obj.race === race.id && obj.scenario === scenario.id)[0];
        objective3 = data.objectives.filter(obj => obj.stage === 3 && obj.race === race.id && obj.scenario === scenario.id)[0];
    } else {
        objective1 = getRandomItem(data.objectives.filter(obj => obj.stage === 1 && obj.race === race.id));
        objective2 = getRandomItem(data.objectives.filter(obj => obj.stage === 2 && obj.race === race.id));
        objective3 = getRandomItem(data.objectives.filter(obj => obj.stage === 3 && obj.race === race.id));
    }

    $('#objectives-container').removeClass('hidden');
    $('#selected-objective-1').text(objective1.name);
    $('#selected-objective-2').text(objective2.name);
    $('#selected-objective-3').text(objective3.name);

    const objective1Additions = data.objectiveSetup[options.playerCount].objective1Additions;
    const objective2Additions = data.objectiveSetup[options.playerCount].objective2Additions;
    const objective3Additions = data.objectiveSetup[options.playerCount].objective3Additions;

    if(objective1Additions === 0) {
        $('#objective-1-additions').addClass('hidden');
    } else {
        $('#objective-1-additions-count').text(objective1Additions);
        $('#objective-1-additions').removeClass('hidden');

        if(objective1Additions === 1) {
            $('#objective-1-additions-plural').addClass('hidden');
        } else {
            $('#objective-1-additions-plural').removeClass('hidden');
        }
    }

    if(objective2Additions === 0) {
        $('#objective-2-additions').addClass('hidden');
    } else {
        $('#objective-2-additions-count').text(objective2Additions);
        $('#objective-2-additions').removeClass('hidden');

        if(objective2Additions === 1) {
            $('#objective-2-additions-plural').addClass('hidden');
        } else {
            $('#objective-2-additions-plural').removeClass('hidden');
        }
    }

    if(objective3Additions === 0) {
        $('#objective-3-additions').addClass('hidden');
    } else {
        $('#objective-3-additions-count').text(objective3Additions);
        $('#objective-3-additions').removeClass('hidden');

        if(objective3Additions === 1) {
            $('#objective-3-additions-plural').addClass('hidden');
        } else {
            $('#objective-3-additions-plural').removeClass('hidden');
        }
    }

    if(race.id === RACE.HUMAN) {
        $('.characters-title').text('Characters');
        $('.objectives-title').text('Objectives/Enemy');
        $('.objective-additions-title').text('Young Blood');
        $('.enemy-phase-name').text('Enemy');
    } else {
        $('.characters-title').text('Armory');
        $('.objectives-title').text('Prey');
        $('.objective-additions-title').text('Mercenary');
        $('.enemy-phase-name').text('Prey');
    }

    $('.race-name').text(race.name);

    const setupTurns = data.objectiveSetup[options.playerCount].setupTurns;

    if(setupTurns > 0) {
        $('#preparation-turn-count').text(setupTurns);
        $('#preparation-turn').removeClass('hidden');

        if(setupTurns === 1) {
            $('#preparation-turn-plural').addClass('hidden');
        } else {
            $('#preparation-turn-plural').removeClass('hidden');
        }
    } else {
        $('#preparation-turn').addClass('hidden');
    }

    let possibleAvatars = JSON.parse(JSON.stringify(data.avatars.filter(avatar => avatar.race === race.id)));

    if(options.limitAvatars) {
        possibleAvatars = possibleAvatars.filter(avatar => (avatar.scenario & scenario.id) === scenario.id);
    }

    let avatars = [];

    while(avatars.length < options.playerCount) {
        let random = Math.floor(Math.random() * possibleAvatars.length);
        avatars.push(possibleAvatars.splice(random, 1)[0]);
    }

    const $template = $('#player-template').children('div').first();

    $('#players').empty();

    const startingPlayer = getRandomNumber(options.playerCount);
 
    avatars.forEach((avatar, index) => {
        let $character = $template.clone();

        $character.find('.player-number').text(index + 1);
        $character.find('.player-avatar').text(avatar.name);
        $character.find('.player-role-plural').text(avatar.roleCards.length > 1 ? 's' : '');
        $character.find('.player-roles').text(avatar.roleCards.join(', '));

        if(index == startingPlayer) {
            $character.find('.starting-player').removeClass('hidden');
        }

        $('#players').append($character);
    });


    let possibleCharacters = JSON.parse(JSON.stringify(data.characters.filter(char => char.race === race.id)));

    if(options.limitCharacters) {
        possibleCharacters = possibleCharacters.filter(char => char.scenario === scenario.id);
    }

    characters = [];

    while(characters.length < 4) {
        let random = Math.floor(Math.random() * possibleCharacters.length);
        characters.push(possibleCharacters.splice(random, 1)[0]);
    }

    characters.sort((a, b) => {
        return b.name < a.name ? 1 : -1;
    });

    $('#characters').text(characters.map(char => char.name).join(', '));

    $('#output').removeClass('hidden');
}


function updateOptionVisibility(evt) {
    if(parseInt($(evt.target).val(), 10) === SCENARIO.NONE) {
        $('.theme-only').addClass('hidden');
    } else {
        $('.theme-only').removeClass('hidden');
    }
}

function drawChart() {
    if(chart === null) {
        chart = new google.visualization.PieChart(document.getElementById('piechart'));
    }

    const classes = {
        intel: 0,
        leadership: 0,
        strength: 0,
        survival: 0,
        tech: 0
    };

    characters.forEach(char => {
        Object.keys(char.classes).forEach(thisClass => {
            classes[thisClass] += char.classes[thisClass];
        });
    });

    
    const data = google.visualization.arrayToDataTable([
        ['Class', 'Card count'],
        ['Intel', classes.intel],
        ['Leadership', classes.leadership],
        ['Strength', classes.strength],
        ['Survival', classes.survival],
        ['Tech', classes.tech]
      ]);

      var options = {
        colors: [
            '#ffc300',
            '#00aaff',
            '#00aa00',
            '#cc0000',
            '#aaaaaa'
        ]
      };

      chart.draw(data, options);

}

function showClassDistribution() {
    $('#class-distribution-modal').modal('show');
    drawChart();
}


/**
 * Initialize the state of inputs
 */
function initialize() {
    google.charts.load('current', {'packages':['corechart']});
    google.charts.setOnLoadCallback(() => {
        respectDarkMode();

        $('[data-toggle="tooltip"]').tooltip({
            container: 'body'
        });

        $('#save').click(saveState);
        $('#load').click(loadState);
        $('#generate').click(generateGame);
        $('#view-classes').click(showClassDistribution);
        $('#match-scenario').change(updateOptionVisibility);

        loadState();
        populateChangelog($('#changelog-accordion'), $('#changelog-template'), $('#last-updated'), changelog);
        setInterval(respectDarkMode, 1000);
    });
}


initialize();
