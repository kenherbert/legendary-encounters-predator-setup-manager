# Legendary Encounters: Predator setup manager

An automated game setup manager for Legendary Encounters: Predator.

Please note that this was made for a section of the HTML code to be copy/pasted into a flatpage in my [Django portfolio](https://gitlab.com/kenherbert/developer-portfolio) but is setup as a standalone page with no requirement for Python or Django for completeness.
